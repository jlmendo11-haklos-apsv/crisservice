package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Publication;

public interface PublicationDAO {
	Publication create( Publication publication );
	Publication read( String publicationId );
	Publication update( Publication publication );
	Publication delete( Publication publication );

	List<Publication> readAll();

	List<Publication> readAllPublications(String researcherId);
}
