package es.upm.dit.apsv.cris.dao;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.Assert;

import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Researcher;

class ResearcherDAOImplementationTest {
	  private Researcher r;
	  private ResearcherDAO rdao;

	  @BeforeAll
	  static void dbSetUp() throws Exception {
	        CSV2DB.loadPublicationsFromCSV("publications.csv");
	        CSV2DB.loadResearchersFromCSV("researchers.csv");
	   }

	  @BeforeEach
	  void setUp() throws Exception {
	        rdao = ResearcherDAOImplementation.getInstance();
	        r = new Researcher();
	        r.setEmail("12345343100");
	        r.setId("12345343100");
	        r.setLastname("Ferreiros");
	        r.setName("Javier");
	        r.setPassword("1234");
	        r.setScopusURL("https://www.scopus.com/authid/detail.uri?authorId=12345343100");
	  }
	  
	  @Test
	  void testCreate() {
	          rdao.delete(r);
	          rdao.create(r);
	          Assert.assertEquals(r, rdao.read(r.getId()));
	  }

	  @Test
	  void testRead() {
		  Assert.assertEquals(r, rdao.read(r.getId()));
	  }

	  @Test
	  void testUpdate() {
	          String oldpwd = r.getPassword();
	          r.setPassword("1111");
	          rdao.update(r);
	          Assert.assertEquals(r, rdao.read(r.getId()));
	          r.setPassword(oldpwd);
	          rdao.update(r);
	  }
	  @Test
	  void testDelete() {
	          rdao.delete(r);
	          Assert.assertNull(rdao.read(r.getId()));
	          rdao.create(r);
	  }
	  @Test
	  void testReadAll() {
		  Assert.assertTrue(rdao.readAll().size() > 75);
	  }
	  @Test
	  void testReadByEmail() {
	          Assert.assertEquals(r, rdao.readByEmail(r.getEmail()));
	  }
}
